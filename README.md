## Gmail Extractor - Solving BNI

This script aims to help developer from doing  manually download
BNI attachment one by one to solving BNI issues.

### How to run

<pre>
$ go run main.go retrieve [accountNo] [backdate days] [desired output folder]
</pre>

or you can run the executable by creating the binary:
<pre>
$ go build 
$ go install gmail-extractor
</pre>

and run the binary:
<pre>
$ gmail-extractor retrieve [accountNo] [backdate days] [desired output folder]
</pre>

in Windows:

Doesn't need to build and install,
just copy the gmail-extractor.exe and run with the following format:

<pre>
$ ./gmail-extractor.exe retrieve [accountNo] [backdate days] [desired output folder]
</pre>
package cmd

import (
	"fmt"
	"github.com/LDCS/ezGmail"
	"github.com/spf13/cobra"
	"log"
	"strings"
)

var retrieveByDateCmd = &cobra.Command{
	Use:   "retrieveByDate",
	Short: "Retrieve bank statement with args: [account_no] [since] [until] [output_folder]",
	Long:  `Retrieve bank statement from email. For example: go main.go retrieveByDate 0895382829 40 output`,
	Run: func(cmd *cobra.Command, args []string) {
		findByDateRange(args[0], args[1]+"d", args[2]+"d", args[3])
	},
}

func init() {
	rootCmd.AddCommand(retrieveByDateCmd)
}

func findByDateRange(accountNo string, since string, until string, outputPath string) {

	var gs ezGmail.GmailService
	gs.From(since).To(until).Match(accountNo).HasAttachment(true)

	for _, ii := range gs.GetMessages() {
		fmt.Println("\nSubject")
		if ii.HasSubject() {
			fmt.Println(ii.GetSubject())
		}
		fmt.Println("\nAttachments")
		if ii.HasAttachments() {
			for _, jj := range ii.GetAttachments() {
				if strings.Contains(jj.GetFilename(), accountNo) {
					_, err := saveFile(jj, outputPath)
					if err != nil {
						log.Fatalf("error when saving file")
					}
				}
			}
		}
	}

}

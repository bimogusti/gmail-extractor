/*
Copyright © 2022
*/
package cmd

import (
	"fmt"
	"github.com/LDCS/ezGmail"
	"github.com/spf13/cobra"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// retrieveCmd represents the retrieve command
var retrieveCmd = &cobra.Command{
	Use:   "retrieve",
	Short: "Retrieve bank statement with args: [account_no] [no_of_days] [output_folder]",
	Long:  `Retrieve bank statement from email. For example: go main.go retrieve 0895382829 40 output`,
	Run: func(cmd *cobra.Command, args []string) {
		findAttachment(args[0], args[1]+"d", args[2])
	},
}

func createFolder(name string) {
	err := os.Mkdir(name, 0755)
	if err != nil {
		log.Fatal("Error creating folder: already exist")
	}
}

func init() {
	rootCmd.AddCommand(retrieveCmd)
}

// param -> example: accountNo='1172018447', days='40d'
func findAttachment(accountNo string, days string, outputPath string) {
	file, _ := filepath.Abs(outputPath)
	createFolder(file)
	var gs ezGmail.GmailService
	gs.InitSrv()

	gs.InInbox().NewerThanRel(days).Match(accountNo).HasAttachment(true)

	for _, ii := range gs.GetMessages() {
		fmt.Println("\nSubject")
		if ii.HasSubject() {
			fmt.Println(ii.GetSubject())
		}
		fmt.Println("\nAttachments")
		if ii.HasAttachments() {
			for _, jj := range ii.GetAttachments() {
				if strings.Contains(jj.GetFilename(), accountNo) {
					_, err := saveFile(jj, outputPath)
					if err != nil {
						log.Fatalf("error when saving file")
					}
				}
			}
		}
	}
}

func saveFile(jj *ezGmail.GmailAttachment, outputPath string) (string, error) {
	file, _ := filepath.Abs(outputPath)
	f, err := os.Create(string(file) + "/" + jj.GetFilename())
	check(err)

	defer f.Close()

	_, err = f.WriteString(string(jj.GetData()))
	check(err)
	return "save to folder bulk_files", nil
}

func check(e error) {
	if e != nil {
		log.Fatal("Something went wrong")
	}
}
